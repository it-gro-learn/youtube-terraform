# LearnLinuxTV
* https://www.youtube.com/user/JtheLinuxguy
* http://www.learnlinux.tv
* https://github.com/LearnLinuxTV
* https://twitter.com/jaythelinuxguy
* https://jaylacroix.com/

## Terraform
* https://www.youtube.com/c/LearnLinuxtv/search?query=terraform
* [Automating_Server_Deployments_with_Terraform](Automating_Server_Deployments_with_Terraform)

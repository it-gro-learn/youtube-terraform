provider "linode" {
    token = "d95ab522b717f75439d6a84455eb4239dae836d73419ff3a6dc1067e9d7de834"
}

resource "linode_instance" "my_linode" {
    label = "my_linode"
    image = "linode/debian10"
    region = "us-east"
    type = "g6-nanode-1"
    root_pass = "Supersecret!!!"
}

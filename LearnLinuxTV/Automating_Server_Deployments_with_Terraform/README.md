# Automating Linux Server Deployments with Terraform and Linode
* https://www.youtube.com/watch?v=_nb6Rqc3MEQ
* https://wiki.learnlinux.tv/index.php/Automating_Linux_Server_Deployments_with_Terraform_and_Linode
* [screenshots](screenshots)
* https://www.terraform.io/downloads.html

```
# sudo apt install unzip
```

```
curl -LO https://releases.hashicorp.com/terraform/0.14.3/terraform_0.14.3_linux_amd64.zip
unzip terraform_*_linux_amd64.zip
rm terraform_*_linux_amd64.zip

sudo mv terraform    /usr/local/bin/
sudo chown root:root /usr/local/bin/terraform
```

```
Usage: terraform [global options] <subcommand> [args]

The available commands for execution are listed below.
The primary workflow commands are given first, followed by
less common or more advanced commands.

Main commands:
  init          Prepare your working directory for other commands
  validate      Check whether the configuration is valid
  plan          Show changes required by the current configuration
  apply         Create or update infrastructure
  destroy       Destroy previously-created infrastructure

All other commands:
  console       Try Terraform expressions at an interactive command prompt
  fmt           Reformat your configuration in the standard style
  force-unlock  Release a stuck lock on the current workspace
  get           Install or upgrade remote Terraform modules
  graph         Generate a Graphviz graph of the steps in an operation
  import        Associate existing infrastructure with a Terraform resource
  login         Obtain and save credentials for a remote host
  logout        Remove locally-stored credentials for a remote host
  output        Show output values from your root module
  providers     Show the providers required for this configuration
  refresh       Update the state to match remote systems
  show          Show the current state or a saved plan
  state         Advanced state management
  taint         Mark a resource instance as not fully functional
  untaint       Remove the 'tainted' state from a resource instance
  version       Show the current Terraform version
  workspace     Workspace management

Global options (use these before the subcommand, if any):
  -chdir=DIR    Switch to a different working directory before executing the
                given subcommand.
  -help         Show this help output, or the help for a specified subcommand.
  -version      An alias for the "version" subcommand.
```



```
mkdir terraform_wd
cd terraform_wd

nano linode_example_1.tf
```


`linode_example_1.tf`
```
provider "linode" {
    token = "d95ab522b717f75439d6a84455eb4239dae836d73419ff3a6dc1067e9d7de834"
}

resource "linode_instance" "my_linode" {
    label = "my_linode"
    image = "linode/debian10"
    region = "us-east"
    type = "g6-nanode-1"
    root_pass = "Supersecret!!!"
}
```

```
terraform init
terraform plan
terraform apply
```


```
cd ..
mkdir terraform_wd_2
cd terraform_wd_2
cp ../terraform_wd/linode_example_1.tf linode_example_2.tf

nano linode_example_2.tf
```


`linode_example_2.tf`
```
variable "linode-api-key" {}
variable "root-password" {}

provider "linode" {
    token = var.linode-api-key
}

resource "linode_instance" "my_linode_2" {
    label = "my_linode_2"
    image = "private/7692309"
    region = "us-east"
    type = "g6-nanode-1"
    root_pass = var.root-password
}
```

`terraform.tfvars`
```
linode-api-key="d95ab522b717f75439d6a84455eb4239dae836d73419ff3a6dc1067e9d7de834"
root-password="Supersecret!!!"
```


`.gitignore`
```
terraform.tfvars
terraform.tfstate
```


```
terraform init
terraform plan
terraform apply

terraform destroy
```

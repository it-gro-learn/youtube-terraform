variable "linode-api-key" {}
variable "root-password" {}

provider "linode" {
    token = var.linode-api-key
}

resource "linode_instance" "my_linode_2" {
    label = "my_linode_2"
    image = "private/7692309"
    region = "us-east"
    type = "g6-nanode-1"
    root_pass = var.root-password
}

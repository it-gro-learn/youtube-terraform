# Terraform Course - Automate your AWS cloud infrastructure
  * https://www.youtube.com/watch?v=SLB_c_ayRMo
  * https://github.com/Sanjeev-Thiyagarajan/Terraform-Crash-Course

```
Course Contents
0:00:00 Intro
0:01:54 AWS Setup
0:05:59 Windows Setup
0:10:04 Mac Setup
0:13:11 Linux Install
0:17:39 VSCode
0:20:51 Terraform Overview
0:43:31 Modifying Resources
0:50:30 Deleting Resources
0:54:55 Referencing Resources
1:04:47 Terraform Files
1:09:45 Practice Project
1:50:32 Terraform State Commands
1:54:05 Terraform Output
2:00:39 Target Resources
2:03:46 Terraform Variables
```

mkdir 01_Intro
mkdir 02_AWS_Setup
mkdir 03_Windows_Setup
mkdir 04_Mac_Setup
mkdir 05_Linux_Install
mkdir 06_VSCode
mkdir 07_Terraform_Overview
mkdir 08_Modifying_Resources
mkdir 09_Deleting_Resources
mkdir 10_Referencing_Resources
mkdir 11_Terraform_Files
mkdir 12_Practice_Project
mkdir 13_Terraform_State_Commands
mkdir 14_Terraform_Output
mkdir 15_Target_Resources
mkdir 16_Terraform_Variables

